package com.doug.expertuschallenge.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import com.doug.expertuschallenge.contract.ExpertusContract;
import com.doug.expertuschallenge.model.News;
import com.doug.expertuschallenge.model.Page;

//This class encapsulates Hello Expertus basic features needed for FrontController(coupled with Webpages views through JSP pages)
//and MicroServiceController(REST API) for light weight http client relying on micro web services(curl, postman chrome addon)
//to act on machine readable data such as JSON
@Service("BackendService")
@Transactional
public class BackendService implements ExpertusContract {
    private Page page;
    
    public BackendService() {
    	this.page = new Page();
    	page.setTitle("Hello Expertus WebApp");
    	page.setHeader("Expertus & Our Solutions");
    	String body = "Our cloud-based Payment Factory uses advanced technology to increase cash flow"
    			+ " visibility and lower operating costs.t is a multi-bank global treasury system that"
    			+ " facilitate domestic and cross border payments, cash pooling, forecasting, liquidity "
    			+ "management, Payment on behalf of (POBO) and SWIFT connectivity.";
    	page.setBody(body);
    	initNews();
    	
    }
    
    private void initNews(){
    	News news1= new News();
    	news1.setName("News1");
    	news1.setBody("Daily News1");
    	this.addNews(news1);
    	
    	News news2= new News();
    	news2.setName("News2");
    	news2.setBody("Daily News2");
    	this.addNews(news2);
    }

	@Override
	public Page getPage() {
		// TODO Auto-generated method stub
		return this.page;
	}

	@Override
	public String updatePage(Page pg) {
		// TODO Auto-generated method stub
		this.page=pg;
		return "{status:Page Updated}";
	}

	@Override
	public List<News> retrieveNews() {
		// TODO Auto-generated method stub
		return this.page.getNewsList();
	}

	
	
	@Override
	public String addNews(News news) {
		//using Object Hashcode as Id to guarantee the uniqueness of news Id in the list instead of index usage
		//Using  index as Id is not a good aproach in the case of deletion as 
		//deleting a news will require to adjust all the ids which will increate the time Runtime complexity of delete operation to O(N)
    	news.setId(news.hashCode());
		this.retrieveNews().add(news);
		// TODO Auto-generated method stub
		return "{status:OK}";
		
	}

	@Override
	public String updateNews(News news) {
		// TODO Auto-generated method stub
		News oldNews=this.retrieveNewsbyId(news.getId());
		String status="";
		if(oldNews!=null) {
			oldNews.setName(news.getName());
			oldNews.setBody(news.getBody());
			status = "Updated";
		}
		else {
			status="Not Updated";
		}
		return "{status:"+status+"}";
	}

	@Override
	public boolean deleteNews(News news) {
		// TODO Auto-generated method stub
		if(news ==  null) { //If news is null ,then news is invalid return false
			return false;
		}
		
		News newsMatch=this.retrieveNewsbyId(news.getId());
		boolean status = this.retrieveNews().remove(newsMatch);
		return status;		
	}

	@Override
	public News retrieveNewsbyId(int id) {
		// TODO Auto-generated method stub
		List<News> newsList=this.retrieveNews();
		News theNews = newsList.stream()
						.filter( news -> news.getId() == id)
						.findFirst().orElse(null);
						//.collect(Collectors.toList());
		if(theNews!=null) {
			System.err.println("The news ID found = "+theNews.getId() + " , News title = "+theNews.getName());
		}
		else {System.err.println("No news found for ID = "+id);}
		return theNews;
	}
    
    
    

}
