package com.doug.expertuschallenge.contract;

import java.util.List;

import com.doug.expertuschallenge.model.News;
import com.doug.expertuschallenge.model.Page;

/**
 * Expertus contract is the interface contract that exposes the list of feature operations needed for Expertus Hello World page **/
public interface ExpertusContract {
	public Page getPage() ;
	public String updatePage(Page pg);
	public List<News> retrieveNews() ;
	public News retrieveNewsbyId(int id);
	public String addNews(News news) ;
	public String updateNews(News news);
	public boolean deleteNews(News news);
}
