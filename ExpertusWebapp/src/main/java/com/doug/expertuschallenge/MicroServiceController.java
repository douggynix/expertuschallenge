package com.doug.expertuschallenge;

import java.util.Enumeration;
import java.util.List;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.doug.expertuschallenge.model.News;
import com.doug.expertuschallenge.model.Page;
import com.doug.expertuschallenge.service.BackendService;

//This Controller class exposes Hello Expertus Core features through REST Web Services such as getting Page Info, edit page Info
//add News, Update News, delete News
@RestController
public class MicroServiceController {
	
	@Autowired
	private BackendService backend;
	
	@RequestMapping(value = "/api/page", method = RequestMethod.GET)
	public Page getPage() {
		return backend.getPage();
	}
	
	
	@RequestMapping( value = "/api/updatePage", method = RequestMethod.POST)
	public String editPage(HttpServletRequest request, Model model) {
		Page page = this.backend.getPage();
		 
		String title = request.getParameter("title");
		String header = request.getParameter("header");
		String body = request.getParameter("body");
		
		if(header == null || title == null) {
			return "{status:Page not Updated. 'header' field must not be empty }";
		}
		
		page.setTitle(title);
		page.setHeader(header);
		if( body !=null ) {
			page.setBody(body);
		}

		
		return backend.updatePage(page);	
	}
	
	@RequestMapping(value = "/api/news", method = RequestMethod.GET)
	public List<News> getAllNews(ModelMap model){
		return backend.retrieveNews();
	}
	
	@RequestMapping(value = "/api/deleteNews/{newsId}" , method = RequestMethod.POST)
	public String deleteNews(@PathVariable("newsId") int newsId) {
		News news = backend.retrieveNewsbyId(newsId);
		boolean status=backend.deleteNews(news);
		String resp = (status) ? "Deleted" : "Not Deleted";
		return "{status:"+resp + "}";
	}
	
	@RequestMapping(value = "/api/addNews" , method = RequestMethod.POST)
	public String addNews(HttpServletRequest request){
		String name = request.getParameter("name");
		String body = request.getParameter("body");
		if(name == null) {
			return "{status:News not Added. 'Name' field must not be empty }";
		}
		News news = new News();
		news.setName(name);
		news.setBody(body);
		backend.addNews(news);
		return "{status:News Added}";
	}
	
	
	@RequestMapping(value = "/api/updateNews/{newsId}" , method = RequestMethod.POST)
	public String editNews(HttpServletRequest request ,@PathVariable("newsId") int newsId) {
		String name = request.getParameter("name");
		String body = request.getParameter("body");
		if(name == null) {
			return "{status:News not Added. 'Name' field must not be empty }";
		}
		
		News news = new News();
		news.setId(newsId);
		news.setName(name);
		if(body != null) {
			news.setBody(body);
		}
		return backend.updateNews(news);
	}
	
	
}

