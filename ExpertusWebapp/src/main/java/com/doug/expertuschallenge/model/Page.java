package com.doug.expertuschallenge.model;

import java.util.ArrayList;
import java.util.List;

public class Page {
	private int id;
	private String title;
	private String header;
	private String body;
	private List<News> newsList;
	
	public Page (){
		newsList=new ArrayList<News>();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public List<News> getNewsList() {
		return newsList;
	}
	public void setNewsList(List<News> newsList) {
		this.newsList = newsList;
	}
	
	public void addNews(News news){
		this.newsList.add(news);
	}
	
	
}
