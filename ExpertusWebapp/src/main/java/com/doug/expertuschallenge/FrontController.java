package com.doug.expertuschallenge;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.doug.expertuschallenge.model.News;
import com.doug.expertuschallenge.model.Page;
import com.doug.expertuschallenge.service.BackendService;

@Controller
public class FrontController {
	
	//inject BackendService as a Singleton 
	@Autowired
	BackendService backend;
	
	//Any request to / will redirect to /home
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		//internal re-routing
		return "redirect:home";
		
	}

	//Show Page model value onto JSP home page
	@RequestMapping(value = "/home",method = RequestMethod.GET)
	public String home(ModelMap model) {
		Page page=backend.getPage();
		//Passing Page object as Attribute to the View
		model.addAttribute("pageObj", page);
		return "home";
	}
	
	//Add metadata information to Attribute and make "home" view act as an Admin View
	//Helps avoid duplicating code for the same logic such as Display a user View of the same page
	//and Admin View of it too
	@RequestMapping(value = "/admin",method = RequestMethod.GET)
	public String admin(ModelMap model){
		Page page=backend.getPage();
		model.addAttribute("pageObj", page);
		//isAdmin Attribute set so that view page would show Admin widgets
		//such as buttons for to edit,create new items. See home.jsp for more details 
		model.addAttribute("isAdmin", "true");
		return "home";
	}
	
	// /updatePage uses ModelAndView object to render view which will binds our page model
	//to Spring MVC form entry listed in jsp page pageForm.jsp. See jsp page for details
	@RequestMapping(value = "/updatePage" , method = RequestMethod.GET)
	public ModelAndView updatePage(ModelMap model){
		Page page=backend.getPage();
		//Jsp spring form is waiting for a default "command" attribute to be passed
		//for form entry binding to our Page model
		ModelAndView mdView = new ModelAndView("pageForm", "command", page);
		
		//Btn Action is used to define the text on the submit button.
		//This will ease integrate future code handling the same view to create new page
		//as btn action name will be dynamic
		mdView.addObject("btnAction", "Update");
		//Defines the action page for btnAction . This is a way to dynamically bind a form to an action
		mdView.addObject("actionName", "savePage");
		return mdView;
	}
	
	//Save page update the update with service method provided by backend and return to /admin page
	@RequestMapping(value = "/savePage", method = RequestMethod.POST)
	public String savePage(@ModelAttribute("SpringWeb") Page page, ModelMap model){
		page.setNewsList(backend.getPage().getNewsList());
		backend.updatePage(page);
		return "redirect:admin";
	}
	
	@RequestMapping(value = "/updateNews/{newsId}" , method = RequestMethod.GET)
	public ModelAndView updateNews(@PathVariable("newsId") int newsId, ModelMap model ) {
		System.err.println("News ID : "+newsId);
		News news=this.backend.retrieveNewsbyId(newsId);
		ModelAndView mdView=new ModelAndView("newsForm", "command", news);
		mdView.addObject("btnAction", "update");
		mdView.addObject("actionName", "saveNews");
		return mdView;
	}
	
	@RequestMapping(value = "/saveNews" , method = RequestMethod.POST)
	public String saveNews(@ModelAttribute("SpringWeb") News news, ModelMap model) {
		backend.updateNews(news);
		return "redirect:admin";
	}
	
	@RequestMapping(value = "/deleteNews" , method = RequestMethod.POST)
	public String deleteNews( @ModelAttribute("SpringWeb") News news, ModelMap model) {
		this.backend.deleteNews(news);
		//redirect to Admin Page
		return "redirect:admin";
		
	}
	
	@RequestMapping(value = "/createNews", method = RequestMethod.GET)
	public ModelAndView createNews(ModelMap model) {
		News news= new News();
		ModelAndView mdView = new ModelAndView("newsForm", "command", news) ;
		mdView.addObject("btnAction", "add");
		mdView.addObject("actionName", "addNews");
		return mdView;
	}
	
	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String addNews(@ModelAttribute("SpringWeb") News news, ModelMap model) {
		backend.addNews(news);
		return "redirect:admin";
	}

}
