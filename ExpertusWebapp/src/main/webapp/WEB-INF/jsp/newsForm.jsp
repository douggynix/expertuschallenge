<%@ page contentType = "text/html; charset = UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>${pageObj.title}</title>
  <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet"/>
  <style media="screen" type="text/css">
	body {
  padding-top: 5rem;
}
.starter-template {
  padding: 3rem 1.5rem;
  text-align: center;
}
  </style>
</head>
<body>
	<%@include file="menu.jsp" %>
    <main role="main" class="container">

      <div class="starter-template">
        <h1>${pageObj.header}</h1>
        <p class="lead">${pageObj.body}</p>
      </div>
      
    <div class="container">
    	<div class="hello-form">
    		<c:url value="/${actionName}" var="actionUrl" />
		    <form:form action="${actionUrl}" >
		    
			  <div class="form-group">
			    <form:label path="name" for="name">Name</form:label>
			     <form:input path="name" class="form-control"  placeholder="Header Headline"/>
			  </div>
			  <div class="form-group">
			    <form:label path="body" for="body">Body</form:label>
			    <form:textarea path="body" class="form-control" rows="3"></form:textarea>
			  </div>
			  <form:hidden path="id" for="id"/>
			  <form:hidden path="pageId" for="pageId"/>
			  <br />
			  <c:choose>
				  <c:when test="${btnAction == 'update'}">
					  <button type="submit" class="btn btn-primary">${btnAction}</button> 
					  &nbsp;&nbsp;
					  <c:url value="/deleteNews" var="deleteActionUrl"/>
					  <button type="button" class="btn btn-secondary" onkeypress="deleteNews('command','${deleteActionUrl}')" onclick="deleteNews('command','${deleteActionUrl}')">delete</button>
				  </c:when>
				  <c:otherwise>
				  	<button type="submit" class="btn btn-primary">create</button>
				  </c:otherwise>
			  </c:choose>

			</form:form>
		
		</div>
	</div>
        
</body>
<script type="text/javascript" src='<c:url value="/resources/js/jquery-3.2.1.min.js"></c:url>'></script>

<script type="text/javascript">
	//Delete function enable to use the form to change its action url dynamically to point to deleteNews web service
	function deleteNews(form_id,deleteUrl) {
		theForm=document.getElementById(form_id);
		theForm.action=deleteUrl;
		theForm.submit();
	}
</script>
</html>