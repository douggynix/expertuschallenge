<%@ page contentType = "text/html; charset = UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>${pageObj.title}</title>
  <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet"/>
  <style media="screen" type="text/css">
	body {
  padding-top: 5rem;
}
.starter-template {
padding: 1.5rem;
    margin-right: 0;
    margin-bottom: 0;
    margin-left: 0;
    border-width: .2rem;	
        bordersolid : #f7f7f9;
}
  </style>
</head>
<body>
	<%@include file="menu.jsp" %>

    <main role="main" class="container">

      <div class="starter-template">
        <h1>${pageObj.header}</h1>
        <p class="lead">${pageObj.body}</p>
        
        <c:if test="${not empty isAdmin}">
       		<c:url value="updatePage" var="editPageUrl"></c:url>
        	<p><a class="btn btn-secondary" href="${editPageUrl}" role="button">Edit Page Info</a></p>
        </c:if>
        
      </div>
      
    <div class="container">
       <c:if test="${not empty isAdmin}">
       				<br/>
       				<c:url value="/createNews" var="createNewsUrl"></c:url>
	            	<p><a class="btn btn-primary" href="${createNewsUrl}" role="button">Add News</a></p>
	            	<br/>
	   </c:if>
        
      <div class="row">
	        <c:forEach var="news" items="${pageObj.newsList}">
		          <div class="col-md-4">
		            <h2>${news.name }</h2>
		            <p>${news.body}</p>
		            <c:if test="${not empty isAdmin}">
	       				<c:url value="/updateNews" var="editNewsUrl"></c:url>
	        			<p><a class="btn btn-secondary" href="${editNewsUrl}/${news.id}" role="button">Modify</a></p>
	        		</c:if>
		          </div>
	        </c:forEach>
      </div>
        
</body>
<script type="text/javascript" src='<c:url value="/resources/js/jquery-3.2.1.min.js"></c:url>'></script>
</html>