<%@ page contentType = "text/html; charset = UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix = "form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>${pageObj.title}</title>
  <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet"/>
  <style media="screen" type="text/css">
	body {
	  padding-top: 5rem;
	}
	.starter-template {
	  padding: 3rem 1.5rem;
	  text-align: center;
	}
	
	.hello-form{
	  font-weight : bold
	}
  </style>
</head>
<body>
	<%@include file="menu.jsp" %>
	
    <main role="main" class="container">

      <div class="myForm">
        <h1>${pageObj.header}</h1>
        <p class="lead">${pageObj.body}</p>
      </div>
      
    <div class="container">
    	<div class="hello-form">
    		<c:url value="/${actionName}" var="actionUrl" />
		    <form:form action="${actionUrl}" >
			  <div class="form-group">
			    <form:label path="header" for="header">Header</form:label>
			     <form:input path="header" class="form-control"  placeholder="Header Headline"/>
			  </div>
			  <div class="form-group">
			    <form:label path="body" for="body">Description</form:label>
			    <form:textarea path="body" class="form-control" rows="3"></form:textarea>
			  </div>
			  <form:hidden path="title" for="title"/>
			  <br />
			  <button type="submit" class="btn btn-primary">${btnAction}</button>
			</form:form>
		</div>
    </div>
        

        
</body>
<script type="text/javascript" src='<c:url value="/resources/js/jquery-3.2.1.min.js"></c:url>'></script>
</html>